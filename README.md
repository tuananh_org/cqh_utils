cqh_utils
======================

A plugin for [Discourse](http://discourse.org)

Installation
============

* Run `rake plugin:install repo=https://tuananh_org@bitbucket.org/tuananh_org/cqh_utils.git` in your discourse directory
* In development mode, run `rake assets:clean`
* In production, recompile your assets: `rake assets:precompile`

License
=======
MIT
