# name: cqh_utils
# about: A small plugin to customize discourse for personal needs.
# version: 0.1
# authors: Tuan Anh Tran

register_asset("javascripts/clean_markdown.js", :server_side)
register_asset('javascripts/custom_nav_item.js', :server_side)